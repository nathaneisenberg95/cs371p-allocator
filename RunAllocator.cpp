// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <sstream>

#include <cmath>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <sstream>
#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

#include "Allocator.hpp"

const int OBJSIZE = 8;
const int HEAPSIZE = 1000;
const int INTHEAP = 1000/4;

class my_heap {
    private:
        int heap [INTHEAP];
    
    public:
        my_heap() {
            heap[0] = 1000 - 8;
            for (int i = 1; i < INTHEAP-1; i++)
                heap[i] = 0;
            heap[INTHEAP-1] = 1000 - 8;
        }
    
        void print_heap() {
            int i = 0;
            int hold = 0;
            bool first = true;
            while (i < INTHEAP) {
                if (!first)
                    std::cout << " ";
                hold = heap[i];
                std::cout << hold;
                if (hold < 0)
                    hold = hold * -1;
                hold = hold / 4;
                hold = hold + 2;
                i = i + hold;
                first = false;
            }
            std::cout << "\n";
        }
        
        // return -1 if failed
        // 16 is minimum
        int allocate(int num_bytes) {
            if (num_bytes <= 0)
                return -1;
            // find block
            int i = 0;
            while (i < INTHEAP) {
                if (heap[i] >= num_bytes)
                    break;
                if (heap[i] < 0)
                    i = i + (-1 * (heap[i]/4)) + 2;
                else
                    i = i + (heap[i]/4) + 2;
            }
            if (i >= INTHEAP)
                return -1;
            
            // check if we can split
            if (heap[i] - num_bytes >= 16) {
                // set sentinels
                int hold = heap[i];
                // first block
                heap[i] = num_bytes * -1;
                heap[i + (num_bytes / 4) + 1] = heap[i];
                
                // second block
                i = i + (num_bytes / 4) + 2;
                int new_size = hold - num_bytes - 8;
                heap[i] = new_size;
                heap[i + (new_size / 4) + 1] = heap[i];
            }
            else {
                // set sentinels
                heap[i] = heap[i] * -1;
                heap[i + (heap[i] / 4) + 1] = heap[i];
            }
            return 0;
        }
    
        // return 0 if ok
        // return -1 if invalid
        // which_block is negative
        int deallocate(int which_block) {
            // find block
            int i = 0;
            while (i < INTHEAP) {
                if (heap[i] < 0)
                    which_block++;
                if (which_block == 0)
                    break;

                i = i + abs(heap[i]/4) + 2;
            }
            if (i >= INTHEAP)
                return -1;
            
            heap[i] = heap[i] * -1;
            heap[i + heap[i]/4 + 1] = heap[i];
            // both left and right are free
            if (i > 0 && heap[i-1] > 0 && i + (heap[i]/4) + 2 < INTHEAP && heap[i + (heap[i]/4) + 2] > 0) {
                int hold1 = heap[i-1];
                int hold2 = heap[i];
                int hold3 = heap[i + (heap[i]/4) + 2];
                int new_size = hold1 + hold2 + hold3 + 16;
                
                heap[i - 2 - hold1/4] = new_size;
                heap[i + hold2/4 + hold3/4 + 3] = new_size;
            }
            // left block free
            else if (i > 0 && heap[i-1] > 0) {
                int hold1 = heap[i-1];
                int hold2 = heap[i];
                int new_size = hold1 + hold2 + 8;
                
                heap[i - 2 - hold1/4] = new_size;
                heap[i + 1 + hold2/4] = new_size;
            }
            // right block is free
            else if (i + (heap[i]/4) + 2 < INTHEAP && heap[i + (heap[i]/4) + 2] > 0) {
                int hold1 = heap[i];
                int hold2 = heap[i + (heap[i]/4) + 2];
                int new_size = hold1 + hold2 + 8;
                
                heap[i] = new_size;
                heap[i + new_size/4 + 1] = new_size;
            }
            
            return 0;
        }
};

// ----
// main
// ----

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */
   int numCases = 0;
    cin >> numCases;
    
    string line;
    getline(cin, line);
    getline(cin, line);
    int action;
    my_heap current = my_heap();
    while (getline(cin, line)) {
        if (line.compare("") == 0) {
            // output singular test case result
            current.print_heap();
            // reset heap
            current = my_heap();
        }
        else {
            
            // either allocate or deallocate
            stringstream hold(line);
            hold >> action;
            if (action < 0) {
                // deallocate (need to check if valid)
                current.deallocate(action);
            }
            else if (action == 0) {
                // not valid
            }
            else {
                action = action * OBJSIZE; // # of bytes wanted
                // allocate
                current.allocate(action);
            }
        }
    }
    current.print_heap();
    return 0;
}
