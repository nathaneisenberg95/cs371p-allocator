// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;}                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);}

    public:
        // --------
        // typedefs
        // --------

        using      value_type = T;

        using       size_type = std::size_t;
        using difference_type = std::ptrdiff_t;

        using       pointer   =       value_type*;
        using const_pointer   = const value_type*;

        using       reference =       value_type&;
        using const_reference = const value_type&;

    public:
        // ---------------
        // iterator
        // over the blocks
        // ---------------

        class iterator {
            // -----------
            // operator ==
            // -----------

            friend bool operator == (const iterator& lhs, const iterator& rhs) {
                // <your code>
                return (lhs == rhs);}                                           // replace!

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const iterator& lhs, const iterator& rhs) {
                return !(lhs == rhs);}

            // -----------
            // operator <
            // -----------

            friend bool operator < (const iterator& lhs, const iterator& rhs) {
                return (lhs._p < rhs._p);} // QUESTIONABLE

            private:
                // ----
                // data
                // ----

                int* _p;

            public:
                // -----------
                // constructor
                // -----------

                iterator (int* p) {
                    _p = p;}

                // ----------
                // operator *
                // ----------

                int& operator * () const {
                    // <your code>
                    return *_p;}           // replace!

                // -----------
                // operator ++
                // -----------

                iterator& operator ++ () {
                    // <your code>
                    int hold = *_p;
                    hold = hold / 4;
                    hold = hold + 2;
                    _p = _p + hold;
                    return *this;}

                // -----------
                // operator ++
                // -----------

                iterator operator ++ (int) {
                    iterator x = *this;
                    ++*this;
                    return x;}

                // -----------
                // operator --
                // -----------

                iterator& operator -- () {
                    // <your code>
                    int hold = *(_p - 1);
                    hold = hold / 4;
                    hold = hold + 2;
                    _p = _p - hold;
                    return *this;}

                // -----------
                // operator --
                // -----------

                iterator operator -- (int) {
                    iterator x = *this;
                    --*this;
                    return x;}};

        // ---------------
        // const_iterator
        // over the blocks
        // ---------------

        class const_iterator {
            // -----------
            // operator ==
            // -----------

            friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
                // <your code>
                return (lhs == rhs);}                                                       // replace!

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
                return !(lhs == rhs);}

            // -----------
            // operator <
            // -----------

            friend bool operator < (const const_iterator& lhs, const const_iterator& rhs) {
                return (lhs._p < rhs._p);} // QUESTIONABLE

            private:
                // ----
                // data
                // ----

                const int* _p; // might be (int* _p);

            public:
                // ----------
                // operator +
                // ----------
                const int* operator + (const int nums) {
                    return _p + nums; // QUESTIONABLE
                }

                // -----------
                // constructor
                // -----------

                const_iterator (const int* p) {
                    _p = p;}

                // ----------
                // operator *
                // ----------

                const int &operator * () const {
                    // <your code>
                    return *_p;}                 

                // -----------
                // operator ++
                // -----------

                const_iterator& operator ++ () {
                    // <your code>
                    int hold = *_p;
                    hold = hold / 4;
                    hold += 2;
                    _p = _p + hold;
                    return *this;}

                // -----------
                // operator ++
                // -----------

                const_iterator operator ++ (int) {
                    const_iterator x = *this;
                    ++*this;
                    return x;}

                // -----------
                // operator --
                // -----------

                const_iterator& operator -- () {
                    // <your code>
                    int hold = *(_p - 1);
                    hold = hold / 4;
                    hold = hold + 2;
                    _p = _p - hold;
                    return *this;}

                // -----------
                // operator --
                // -----------

                const_iterator operator -- (int) {
                    const_iterator x = *this;
                    --*this;
                    return x;}};

    private:
        // ----
        // data
        // ----

        char a[N];

        // -----
        // valid
        // -----

        /**
         * O(1) in space
         * O(n) in time
         * <your documentation>
         */
        bool valid () const {
            // <your code>
            // <use iterators>
            bool prev_free = false;
            bool free = false;
            for (const_iterator i = begin(); i < end(); i++) {
                // adjacent free blocks?
                if (*i > 0)
                    free = false;
                else {
                    free = true;
                    if (prev_free == true)
                        return false;
                }
                // reset for next loop
                prev_free = free;
                

                // Check for matching sentinels
                // QUESTIONABLE
                int hold = *i;
                int value = hold;
                hold = hold / 4;
                if (*(i + (hold + 1)) != value)
                    return false;
            }
            return true;}

    public:
        // -----------
        // constructor
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
         */
        my_allocator () {
            (*this)[0] = N - 8;
            (*this)[N - 1 - 4] = N - 8;
            // <your code>
            assert(valid());}

        my_allocator             (const my_allocator&) = default;
        ~my_allocator            ()                    = default;
        my_allocator& operator = (const my_allocator&) = default;

        // --------
        // allocate
        // --------

        /**
         * O(1) in space
         * O(n) in time
         * after allocation there must be enough space left for a valid block
         * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
         * choose the first block that fits
         * throw a bad_alloc exception, if n is invalid
         */
        pointer allocate (size_type n) {
            // <your code>
            // first fit block
            iterator i = begin();
            while (i < end()) {
                n++;
                i++;
            }

            // if not found, return nullptr

            // check if splitting is allowed
                // if so, split
            assert(valid());
            // return pointer to block
            return nullptr;}             // replace!

        // ---------
        // construct
        // ---------

        /**
         * O(1) in space
         * O(1) in time
         */
        void construct (pointer p, const_reference v) {
            new (p) T(v);                               // this is correct and exempt
            assert(valid());}                           // from the prohibition of new

        // ----------
        // deallocate
        // ----------

        /**
         * O(1) in space
         * O(1) in time
         * after deallocation adjacent free blocks must be coalesced
         * throw an invalid_argument exception, if p is invalid
         * <your documentation>
         * p is the pointer to the block we are freeing
         * n is irrelevant
         */
        void deallocate (pointer p, size_type n) {
            // <your code>
            if (n < 0)
                p++;
            assert(valid());}

        // -------
        // destroy
        // -------

        /**
         * O(1) in space
         * O(1) in time
         */
        void destroy (pointer p) {
            p->~T();               // this is correct
            assert(valid());}

        // -----------
        // operator []
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         */
        int& operator [] (int i) {
            return *reinterpret_cast<int*>(&a[i]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const int& operator [] (int i) const {
            return *reinterpret_cast<const int*>(&a[i]);}

        // -----
        // begin
        // -----

        /**
         * O(1) in space
         * O(1) in time
         */
        iterator begin () {
            return iterator(&(*this)[0]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator begin () const {
            return const_iterator(&(*this)[0]);}

        // ---
        // end
        // ---

        /**
         * O(1) in space
         * O(1) in time
         */
        iterator end () {
            return iterator(&(*this)[N]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator end () const {
            return const_iterator(&(*this)[N]);}};

#endif // Allocator_h
